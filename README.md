

# IBM i Open Source
This repo will act as the authoritative documentation location for all things open source on IBM i.  If you see a mistake or see a way to make some of the docs better then please issue a pull request.

# Table Of Contents
- [yum](https://bitbucket.org/ibmi/opensource/src/master/docs/yum/)

# Collaboration
- [Questions/comments about this site](https://bitbucket.org/ibmi/opensource/issues).
- [IBMiOSS Wiki](https://bitbucket.org/ibmi/opensource/wiki/Home)

# Links
- [Previous authoritative IBMiOSS site](https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/IBM%20i%20Technology%20Updates/page/Open%20Source%20Technologies)
